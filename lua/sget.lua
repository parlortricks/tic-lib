function sget(x, y)
 local addr = (16384 + (((x // 8) + ((y // 8) * 16)) * 32))
 return peek4(((addr * 2) + (x % 8) + ((y % 8) * 8)))
end
