--constants
local m = math
local sin = m.sin
local cos = m.cos
local pi = m.pi
local tau = 2*pi
local floor = m.floor
local ceil = m.ceil
local rnd = m.random
local t = time
local screen_width = 240
local screen_height = 136
local debug=false
local true_false = {true, false}
local pause=false
--end constants

