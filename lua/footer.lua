--start footer stuff
local rainbow=0
local footer_duration=3
local footer_step=0
local footer_text="Demo #8 by Parlortricks 2021"
local footer_y=126
--end footer stuff

function footer(text)
 local text = text
 local x,y=2,footer_y
 local pxtext=#text*6
 local cx=120-pxtext/2
 circ(cx,y+2,5,10)
 circ(cx,y+2,4,14)
 circ(cx+pxtext-2,y+2,5,10)
 circ(cx+pxtext-2,y+2,4,14)
 rect(cx-2,y-3,pxtext+3,11,10)
 rect(cx-2,y-2,pxtext+3,9,14)
	for k1=-1,1 do
	 for k2=-1,1 do
   print(text,cx+k1,y+k2,12,true)
  end
 end
 print(text,cx,y,4,true)
 for i=cx-1,cx+pxtext do
  for j=y-1,y+6 do
   if pix(i,j)==12 then
    pix(i,j,(i+j+rainbow)/16)
   end
  end
 end
end

function footer_draw()
 if footer_step < 180 then
  footer(footer_text)
  footer_step=footer_step+1
  rainbow = rainbow + 1
 elseif footer_y < 146 then
  footer(footer_text)
 	footer_y=footer_y+1
  rainbow = rainbow + 1
 else
 end
end
