--requires Vector(x,y)
function poly(col, ...)
 local arg = {n=select('#',...),...}
 local pt=nil
 -- arg.n is the real size
 for i = 1,arg.n do
  if i==arg.n then
   pt=1
  else
   pt=i+1
  end
  line(arg[i].x,
       arg[i].y,
       arg[pt].x,
       arg[pt].y,
       col)
 end
end
