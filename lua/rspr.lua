function rspr(sx, sy, scale, angle, mx, my, mw, mh, key, useMap)
  local scale=scale or 1.0
  local angle=angle or 0
  local key=key or 0
  local useMap=useMap or false
  local sv = {{-1, -1, 0, 0},
              {1, -1, 1, 0},
              {-1, 1, 0, 1},
              {1, 1, 1, 1}}
  local scalex = (<<(mw, 2) * scale)
  local scaley = (<<(mh, 2) * scale)
  for p = 1, #sv do
    local _sx = (sv[p][1] * scalex)
    local _sy = (sv[p][2] * scaley)
    local a = ( - angle)
    local rx = ((_sx * cos(a)) - (_sy * sin(a)))
    local ry = ((_sx * sin(a)) + (_sy * cos(a)))
    (sv[p])[1] = (rx + sx)
    (sv[p])[2] = (ry + sy)
    (sv[p])[3] = (<<(mx, 3) + (sv[p][3] * <<(mw, 3)))
    (sv[p])[4] = (<<(my, 3) + (sv[p][4] * <<(mh, 3)))
  end
  textri((sv[1])[1], (sv[1])[2],
         (sv[2])[1], (sv[2])[2],
         (sv[3])[1], (sv[3])[2],
         (sv[1])[3], (sv[1])[4],
         (sv[2])[3], (sv[2])[4],
         (sv[3])[3], (sv[3])[4],
         use_map, key)
  textri((sv[2])[1], (sv[2])[2],
         (sv[3])[1], (sv[3])[2],
         (sv[4])[1], (sv[4])[2],
         (sv[2])[3], (sv[2])[4],
         (sv[3])[3], (sv[3])[4],
         (sv[4])[3], (sv[4])[4],
         use_map, key)
end
