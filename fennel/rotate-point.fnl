(fn rotate-point [p o r]
 "rotate a vector 'p' around an origin 'o' with angle 'r'"
 (let [(px py ox oy r) (values p.x p.y o.x o.y r)
  rx (+ (- (* (- px ox) (cos r)) (* (- py oy) (sin r))) ox)
  ry (+ (+ (* (- py oy) (cos r)) (* (- px ox) (sin r))) oy)
 ]
 {:x rx :y ry})) 

