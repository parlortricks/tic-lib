(fn << [b disp] (math.floor (% (* b (^ 2 (math.floor disp))) (^ 2 32))))

(fn >> [b disp] (math.floor (/ (% b (^ 2 32)) (^ 2 disp))))
