(fn sset [x y c]
 "set sprite sheet pixel"
 (local addr (+ 0x4000 (* (+ (// x 8) (* (// y 8) 16)) 32)))
 (poke4 (+ (* addr 2) (% x 8) (* (% y 8) 8)) c))
